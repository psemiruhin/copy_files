library copy_files.transformer;

import 'dart:async';
import 'package:barback/barback.dart';
import 'dart:io';
import 'package:path/path.dart';

/// Transformer used by `pub build` to copy files from 'build' directory to any directory.
class CopyFilesTransformer extends Transformer {
  final BarbackSettings settings;
  final TransformerOptions options;

  CopyFilesTransformer(BarbackSettings settings) :
    settings = settings,
    options = new TransformerOptions.parse(settings.configuration);

  CopyFilesTransformer.asPlugin(BarbackSettings settings) :
    this(settings);

  Future<bool> isPrimary(AssetId input) {
    bool primary = false;
    if (options.files_in_build != null && options.new_dir != null && options.new_dir != '') {
      for (String fileName in options.files_in_build) {
        if (fileName.contains(input.path)) {
          primary = true;
          break;
        }
      }
    }
    return new Future.value(primary);
  }

  Future apply(Transform transform) {
    String path;

    for (String fileName in options.files_in_build) {
      if (fileName.contains(transform.primaryInput.id.path)) {
        path = options.new_dir + posix.basename(transform.primaryInput.id.path);
        options.files_in_build.remove(fileName);
        break;
      }
    }
    
    File file = new File(path);
    if (transform.primaryInput.runtimeType.toString() == 'BinaryAsset') {
      Stream<List<int>> stream = transform.primaryInput.read();
      return stream.single.then((List<int> bytes) {
        print('created ' + path);
        return file.writeAsBytes(bytes);
      }).catchError((Exception e) {
        print(e.toString());
      });
    } else {
      return transform.primaryInput.readAsString().then((content) {
        return file.create(recursive: true).then((File result) {
          IOSink iosink = file.openWrite(mode: FileMode.WRITE);
          iosink.write(content);
          iosink.close();
          print('created ' + path);
          return;
        });
      }).catchError((Exception e) {
        print(e.toString());
      });
    }
  }
}

class TransformerOptions {
  final List<String> files_in_build;
  final String new_dir;

  TransformerOptions({List<String> this.files_in_build, this.new_dir});

  factory TransformerOptions.parse(Map configuration) {
    config(key, defaultValue) {
      var value = configuration[key];
      return value != null ? value : defaultValue;
    }

    return new TransformerOptions(
        files_in_build: config("files_in_build", []), new_dir: config("new_dir", ""));
  }
}
