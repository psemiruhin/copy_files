Transformer that will copy files from 'build' directory to any directory.

## Usage

Simply add the following lines to your `pubspec.yaml`:

    :::yaml
    dependencies:
      copy_files: any
    transformers:
      - copy_files

## Configuration

You can specify list of files in 'build' directory and new directory:

    :::yaml
    transformers:
      - copy_files:
          files_in_build: 
          - /path/to/file_1
          - /path/to/file_2
          - /path/to/file_n
          new_dir: /new/dir
